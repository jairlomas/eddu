var WINDOW_HEIGHT = 0;
var WINDOW_WIDTH = 0;
var HEADER_HEIGHT = 0;
var FOOTER_HEIGHT = 0;
var slider_home = null;

$(document).ready(function () {

    WINDOW_HEIGHT = $(window).height();
    WINDOW_WIDTH  = $(window).width();
    HEADER_HEIGHT = $(".section_navbar").outerHeight();
    FOOTER_HEIGHT = $(".footer_section").outerHeight();

    if(WINDOW_WIDTH >= 768) {
        var slider_height = parseFloat(WINDOW_HEIGHT) - (HEADER_HEIGHT + FOOTER_HEIGHT);
        $(".slider_section article").css("height", slider_height);
    }else{
        $(".slider_section article").css("height", 500);
    }

    slider_home = $('.slider_home').bxSlider({
        controls: false,
        auto: true,
    });


    $(window).resize(function (e) {

        WINDOW_HEIGHT = $(document).height();
        WINDOW_WIDTH  = $(window).width();
        HEADER_HEIGHT = $(".section_navbar").outerHeight();
        FOOTER_HEIGHT = $(".footer_section").outerHeight();

        if(WINDOW_WIDTH >= 768) {
            var slider_height = parseFloat(WINDOW_HEIGHT) - (HEADER_HEIGHT + FOOTER_HEIGHT);
            slider_home.destroySlider();
            $(".slider_section article").css("height", slider_height);
        }else{
            slider_home.destroySlider();
            $(".slider_section article").css("height", 500);
        }

        slider_home = $('.slider_home').bxSlider({
            controls: false,
            auto: false,
        });

    });


});