<?php namespace MetodikaTI\Http\Controllers\Front;

use MetodikaTI\Article;
use MetodikaTI\Category;
use MetodikaTI\Diccionary;
use MetodikaTI\Http\Requests;
use MetodikaTI\Http\Controllers\Controller;

use Illuminate\Http\Request;

class FrontController extends Controller {

    public function index(){

        return view("front.index");

    }

    public function eddu_te_aconseja(){

        $categories = Category::get();

        return view("front.eddu-te-aconseja", ["categories" => $categories]);
    }

    public function eddu_te_aconseja_by_category($id){

        $category = Category::find($id);
        $articles = Article::where("category_id", $id)->get();

        return view("front.eddu-te-aconseja-by-category", ["articles" => $articles, "category" => $category]);
    }


    public function descripcion_articulo_by_article($article_id){

        $article = Article::find($article_id);
        $category = Category::find($article->category_id);

        return view("front.descripcion-articulo", ["article" => $article, "category" => $category]);
    }


    public function diccionario($type = "", $word = ""){

        if( $type != "" && $word != "" ){

            $data = "";
            if($type == "letra"){
                //Busqueda por letra inicial
                $data = Diccionary::where("word", "regexp", '^['.$word.']+')->get()->toArray();
            }else{

                //Busqueda por palabra completa
                if($word == "all_words"){
                    $data = Diccionary::get()->toArray();
                }else{
                    $data = Diccionary::where("word", "LIKE", "%".$word."%")->get()->toArray();
                }
            }

            return view("front.diccionario", ["type" => $type, "data" => $data, "word" => $word]);
        }else{
            return view("front.diccionario", ["type" => $type, "data" => "", "word" => ""]);
        }

    }


}
