<?php

namespace MetodikaTI\Http\Controllers\Back\Dashboard\Catalog;

use MetodikaTI\Article;
use MetodikaTI\Diccionary;
use MetodikaTI\Http\Requests;
use MetodikaTI\Http\Controllers\Controller;
use MetodikaTI\UserProfile;
use MetodikaTI\Http\Requests\Back\Catalogo\AddDictionaryRequest;
use MetodikaTI\Http\Requests\Back\Catalogo\UpdateDictionaryRequest;
use MetodikaTI\Library\URI;
use MetodikaTI\Category;

use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class DictionaryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $data = Diccionary::get();
        return view('back.dashboard.catalog.dictionary.index', [	'createButton' => Uri::printButton('create', '', 'Nueva Categoria'),
            'permitions' => Uri::checkPermitions(),
            'data'	=>	$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('back.dashboard.catalog.dictionary.create', ['buttonName' => 'Guardar']);
    }


    public function store(AddDictionaryRequest $request){

        $response = [
            'status' => true,
            'mensaje' => ''
        ];


        //Subimos las imagenes
        /*
            palabra
            descripcion
        */

        //Se agrega el nuevo registro a base de datos
        if($response["status"]){

//            $request->palabra = strtolower($request->palabra);
            $data = new Diccionary();
            $data->word	        =	ucfirst(str_replace(":", "", $request->palabra));
            $data->description  =	$request->descripcion;

            //Se verifica si se a guardado el nuevo usuario
            if ($data->save()) {

                $response = [
                    'status' => true,
                    'message' => 'Se ha guardado con éxito el nuevo registro en el sistema.',
                    'url' => '../diccionario'
                ];

            } else {

                $response = [
                    'status' => false,
                    'errors'  => [
                        'No se ha podido guardar el nuevo registro en el sistema, intentelo nuevamente.'
                    ]
                ];

            }

        }

        return response()->json($response);
    }





    public function edit($id)
    {
        $id_decode = base64_decode($id);
        $data = Diccionary::where("id", $id_decode)->first();
        return view('back.dashboard.catalog.dictionary.edit', ['id' => $id, "data" =>  $data]);
    }


    public function update($id, UpdateDictionaryRequest $request)
    {

        $id = base64_decode($id);

        $response = [
            'status' => true,
            'mensaje' => 'Se ha actualziado el registro con éxito.'
        ];


//        $request->palabra = strtolower($request->palabra);
        $data = Diccionary::find($id);
        $data->word	        =	ucfirst(str_replace(":", "", $request->palabra));
        $data->description  =	$request->descripcion;

        if($response["status"]){

            if($data->save()){
                $response = [
                    'status' => true,
                    'message' => 'Se ha actualizado el registro con éxito.',
                    'url' => '../'
                ];
            }else{
                $response = [
                    'status' => false,
                    'errors'  => [
                        'No se ha podido editar el registro'
                    ]
                ];
            }

        }



        return response()->json($response);
    }




    public function destroy($id)
    {

        $response;

        //Se busca el ID
        $data = Diccionary::where('id', '=', base64_decode($id));

        if ($data->count() === 1) {

            //Se borra la cadena
            if ($data->delete(base64_decode($id))) {

                $response = [
                    'status' => true,
                    'message' => 'El registro se ha borrado con exito.',
                    "url" => ""
                ];

            } else {

                $response = [
                    'status' => false,
                    'errors'  => [
                        'No se ha podido eliminar el registro, intentelo nuevamente.'
                    ]

                ];

            }

        } else {

            $response = [
                'status' => false,
                'errors'  => [
                    'No se ha podido borrar el registro.'
                ]

            ];

        }


        echo json_encode($response);
    }


}
