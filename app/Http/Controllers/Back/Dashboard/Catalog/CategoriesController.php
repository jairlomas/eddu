<?php

namespace MetodikaTI\Http\Controllers\Back\Dashboard\Catalog;

use MetodikaTI\Article;
use MetodikaTI\Http\Requests;
use MetodikaTI\Http\Controllers\Controller;
use MetodikaTI\UserProfile;
use MetodikaTI\Http\Requests\Back\Catalogo\AddCategoryRequest;
use MetodikaTI\Http\Requests\Back\Catalogo\UpdateCategoryRequest;
use MetodikaTI\Library\URI;
use MetodikaTI\Category;

use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class CategoriesController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$data = Category::get();
		return view('back.dashboard.catalog.categories.index', [	'createButton' => Uri::printButton('create', '', 'Nueva Categoria'),
												'permitions' => Uri::checkPermitions(),
												'data'	=>	$data]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return view('back.dashboard.catalog.categories.create', ['buttonName' => 'Guardar']);
	}

  
	public function store(AddCategoryRequest $request){

		$response = [
			'status' => true,
			'mensaje' => ''
		];


		//Subimos las imagenes
        /*
            banner_principal
            banner_articulo
            imagen_categoria
            categoria
        */

        $banner_principal_name = md5(date("d-m-Y-H-i-s"))."".random_int(1000,100000)."BP.".$request->file("banner_principal")->getClientOriginalExtension();
        $request->file("banner_principal")->move("assets/front/categorias/", $banner_principal_name);

        //$banner_articulo_name = md5(date("d-m-Y-H-i-s"))."".random_int(1000,100000)."BA.".$request->file("banner_articulo")->getClientOriginalExtension();
        //$request->file("banner_articulo")->move("assets/front/categorias/", $banner_articulo_name);

        $imagen_categoria_name = md5(date("d-m-Y-H-i-s"))."".random_int(1000,100000)."C.".$request->file("imagen_categoria")->getClientOriginalExtension();
        $request->file("imagen_categoria")->move("assets/front/categorias/", $imagen_categoria_name);


		//Se agrega el nuevo registro a base de datos
		if($response["status"]){

			$data = new Category();
			$data->principal_banner	=	"assets/front/categorias/".$banner_principal_name;
            //$data->article_banner	=	"assets/front/categorias/".$banner_articulo_name;
            $data->article_banner	=	"-";
			$data->category_image	=	"assets/front/categorias/".$imagen_categoria_name;
			$data->category_name	=	$request->categoria;

			//Se verifica si se a guardado el nuevo usuario
			if ($data->save()) {

				$response = [
					'status' => true,
					'message' => 'Se ha guardado con éxito el nuevo registro en el sistema.',
                    'url' => '../categorias'
				];

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido guardar el nuevo registro en el sistema, intentelo nuevamente.'
					]
				];

			}

		}

		return response()->json($response);
	}

 
 

 
	public function edit($id)
	{
		$id_decode = base64_decode($id);
		$data = Category::where("id", $id_decode)->first();
		return view('back.dashboard.catalog.categories.edit', ['id' => $id, "data" =>  $data]);
	}

 
	public function update($id, UpdateCategoryRequest $request)
	{
		$id = base64_decode($id);

		$response = [
			'status' => true,
			'mensaje' => 'Se ha actualziado el registro con éxito.'
		];


        $data = Category::find($id);

        if($request->hasFile('banner_principal')){

            //Eliminamos el archivo original
            if(file_exists($data->principal_banner)){
                unlink($data->principal_banner);
            }

            $banner_principal_name = md5(date("d-m-Y-H-i-s"))."".random_int(1000,100000)."BP.".$request->file("banner_principal")->getClientOriginalExtension();
            $request->file("banner_principal")->move("assets/front/categorias/", $banner_principal_name);
            $data->principal_banner	=	"assets/front/categorias/".$banner_principal_name;
        }

//        if($request->hasFile('banner_articulo')) {
//
//            //Eliminamos el archivo original
//            if(file_exists($data->article_banner)){
//                unlink($data->article_banner);
//            }
//
//            $banner_articulo_name = md5(date("d-m-Y-H-i-s")) . "" . random_int(1000, 100000) . "BA." . $request->file("banner_articulo")->getClientOriginalExtension();
//            $request->file("banner_articulo")->move("assets/front/categorias/", $banner_articulo_name);
//            $data->article_banner	=	"assets/front/categorias/".$banner_articulo_name;
//        }

        if($request->hasFile('imagen_categoria')) {

            //Eliminamos el archivo original
            if(file_exists($data->category_image)){
                unlink($data->category_image);
            }

            $imagen_categoria_name = md5(date("d-m-Y-H-i-s")) . "" . random_int(1000, 100000) . "C." . $request->file("imagen_categoria")->getClientOriginalExtension();
            $request->file("imagen_categoria")->move("assets/front/categorias/", $imagen_categoria_name);
            $data->category_image	=	"assets/front/categorias/".$imagen_categoria_name;
        }

        $data->category_name = $request->categoria;

		if($response["status"]){

			if($data->save()){
				$response = [
					'status' => true,
					'message' => 'Se ha actualizado el registro con éxito.',
                    'url' => '../'
				];
			}else{
				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido editar el registro'
					]
				];				
			}

		}

 

		return response()->json($response);
	}

 

 
	public function destroy($id)
	{

		$response;

		//Revisamos si un articulo tiene esa categoria
        $article = Article::where("category_id", base64_decode($id));

        if($article->count() > 0){

            $response = [
                'status' => false,
                'errors'  => [
                    'No se ha podido borrar la categoria, uno o mas articulos estan ligados a esta categoria.'
                ]
            ];

        }else{

            //Se busca el ID
            $data = Category::where('id', '=', base64_decode($id));

            if ($data->count() === 1) {

                $aux_data = $data->first();

                //Se borra la cadena
                if ($data->delete(base64_decode($id))) {

                    //Eliminamos las imagenes
                    if(file_exists($aux_data->principal_banner)){
                        unlink($aux_data->principal_banner);
                    }

//                    if(file_exists($aux_data->article_banner)){
//                        unlink($aux_data->article_banner);
//                    }

                    if(file_exists($aux_data->category_image)){
                        unlink($aux_data->category_image);
                    }

                    $response = [
                        'status' => true,
                        'message' => 'El registro se ha borrado con exito.',
                        "url" => ""
                    ];

                } else {

                    $response = [
                        'status' => false,
                        'errors'  => [
                            'No se ha podido eliminar el registro, intentelo nuevamente.'
                        ]

                    ];

                }

            } else {

                $response = [
                    'status' => false,
                    'errors'  => [
                        'No se ha podido borrar el registro.'
                    ]
                ];

            }

        }
			
		echo json_encode($response);
	}


}
