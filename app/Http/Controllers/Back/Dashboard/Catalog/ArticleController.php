<?php

namespace MetodikaTI\Http\Controllers\Back\Dashboard\Catalog;

use MetodikaTI\Article;
use MetodikaTI\Http\Requests;
use MetodikaTI\Http\Controllers\Controller;
use MetodikaTI\UserProfile;
use MetodikaTI\Http\Requests\Back\Catalogo\AddArticleRequest;
use MetodikaTI\Http\Requests\Back\Catalogo\UpdateArticleRequest;
use MetodikaTI\Library\URI;
use MetodikaTI\Category;

use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class ArticleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $data = Article::get();
        return view('back.dashboard.catalog.articles.index', [	'createButton' => Uri::printButton('create', '', 'Nueva Categoria'),
            'permitions' => Uri::checkPermitions(),
            'data'	=>	$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $categories = Category::pluck('category_name', 'id');
        return view('back.dashboard.catalog.articles.create', ['categories' => $categories, 'buttonName' => 'Guardar']);
    }


    public function store(AddArticleRequest $request){

        $response = [
            'status' => true,
            'mensaje' => ''
        ];


        //Subimos las imagenes
        /*
            category
            imagen_articulo
            titulo
            vista_previa_descripcion
            descripcion
        */


        $imagen_articulo = md5(date("d-m-Y-H-i-s"))."".random_int(1000,100000)."C.".$request->file("imagen_articulo")->getClientOriginalExtension();
        $request->file("imagen_articulo")->move("assets/front/articulos/", $imagen_articulo);

        $banner_articulo_name = md5(date("d-m-Y-H-i-s"))."".random_int(1000,100000)."BA.".$request->file("banner_articulo")->getClientOriginalExtension();
        $request->file("banner_articulo")->move("assets/front/articulos/", $banner_articulo_name);

        //Se agrega el nuevo registro a base de datos
        if($response["status"]){

            $data = new Article();
            $data->category_id	        =	$request->category;
            $data->article_image	    =	"assets/front/articulos/".$imagen_articulo;
            $data->article_banner	    =	"assets/front/articulos/".$banner_articulo_name;
            $data->title	            =	$request->titulo;
            $data->description_preview	=	$request->vista_previa_descripcion;
            $data->content	            =	$request->descripcion;

            //Se verifica si se a guardado el nuevo usuario
            if ($data->save()) {

                $response = [
                    'status' => true,
                    'message' => 'Se ha guardado con éxito el nuevo registro en el sistema.',
                    'url' => '../articulos'
                ];

            } else {

                $response = [
                    'status' => false,
                    'errors'  => [
                        'No se ha podido guardar el nuevo registro en el sistema, intentelo nuevamente.'
                    ]
                ];

            }

        }

        return response()->json($response);
    }





    public function edit($id)
    {
        $id_decode = base64_decode($id);
        $data = Article::where("id", $id_decode)->first();
        $categories = Category::pluck('category_name', 'id');
        return view('back.dashboard.catalog.articles.edit', ['id' => $id, "data" =>  $data, 'categories' => $categories]);
    }


    public function update($id, UpdateArticleRequest $request)
    {

        $id = base64_decode($id);

        $response = [
            'status' => true,
            'mensaje' => 'Se ha actualziado el registro con éxito.'
        ];


        $data = Article::find($id);

        if($request->hasFile('imagen_articulo')){

            //Eliminamos el archivo original
            if(file_exists($data->article_image)){
                unlink($data->article_image);
            }

            $imagen_articulo = md5(date("d-m-Y-H-i-s"))."".random_int(1000,100000)."C.".$request->file("imagen_articulo")->getClientOriginalExtension();
            $request->file("imagen_articulo")->move("assets/front/articulos/", $imagen_articulo);
            $data->article_image = "assets/front/articulos/".$imagen_articulo;
        }

        if($request->hasFile('banner_articulo')) {

            //Eliminamos el archivo original
            if(file_exists($data->article_banner)){
                unlink($data->article_banner);
            }

            $banner_articulo_name = md5(date("d-m-Y-H-i-s")) . "" . random_int(1000, 100000) . "BA." . $request->file("banner_articulo")->getClientOriginalExtension();
            $request->file("banner_articulo")->move("assets/front/articulos/", $banner_articulo_name);
            $data->article_banner	=	"assets/front/articulos/".$banner_articulo_name;
        }

        $data->category_id	        =	$request->category;
        $data->title	            =	$request->titulo;
        $data->description_preview	=	$request->vista_previa_descripcion;
        $data->content	            =	$request->descripcion;

        if($response["status"]){

            if($data->save()){
                $response = [
                    'status' => true,
                    'message' => 'Se ha actualizado el registro con éxito.',
                    'url' => '../'
                ];
            }else{
                $response = [
                    'status' => false,
                    'errors'  => [
                        'No se ha podido editar el registro'
                    ]
                ];
            }

        }



        return response()->json($response);
    }




    public function destroy($id)
    {

        $response;

        //Se busca el ID
        $data = Article::where('id', '=', base64_decode($id));

        if ($data->count() === 1) {

            $aux_data = $data->first();

            //Se borra la cadena
            if ($data->delete(base64_decode($id))) {

                //Eliminamos las imagenes
                if(file_exists($aux_data->article_image)){
                    unlink($aux_data->article_image);
                }

                $response = [
                    'status' => true,
                    'message' => 'El registro se ha borrado con exito.',
                    "url" => ""
                ];

            } else {

                $response = [
                    'status' => false,
                    'errors'  => [
                        'No se ha podido eliminar el registro, intentelo nuevamente.'
                    ]

                ];

            }

        } else {

            $response = [
                'status' => false,
                'errors'  => [
                    'No se ha podido borrar el registro.'
                ]

            ];

        }


        echo json_encode($response);
    }


}
