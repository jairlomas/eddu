<?php namespace MetodikaTI;


use Illuminate\Database\Eloquent\Model;
use MetodikaTI\Category;

class Article extends Model {

    public function category_label()
    {
        return $this->hasOne('MetodikaTI\Category', "id", "category_id");
    }

}
