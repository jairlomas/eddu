<?php namespace MetodikaTI\Library;

use Auth;
use MetodikaTI\SystemModule;
use MetodikaTI\User;
use Permission;
use Session;


class Pastora {


    public static function in_relation($object, $field_id, $field) {
        if (!is_null($object)) {
            if (!is_null($object->$field_id)) {
                if (!is_null($object->$field_id->$field)) {
                    return  true;
                }
            }
        }
        return false;
    }
    /**
     * Metodo que devuelve verdadero si una propiedad se encuentra en un arreglo
     *
     * @return Boolean
     */
    public static function in_object($value,$object) {
        if (is_object($object)) {
            foreach($object as $key => $item) {
                if ($value==$key) return true;
            }
        }
        return false;
    }

    /**
     * Metodo que devuelve los permisos que tiene el usuario
     *
     * @return JSON
     */
    public static function userProfile()
    {
//        dd(Auth::user()->id, User::find(Auth::user()->id));
        return User::find(Auth::user()->id)->userProfile->permits;
    }

    /**
     * Metodo que convierte un Json a Array
     *
     * @return Array
     */
    public static function jsonToArray($json)
    {
        $json = (array)json_decode($json);
        $response = [];
        foreach ($json as $key => $value) {
            $response[(int)$key] = (int)$value;
        }
        return $response;
    }

    /**
     * This method convert the array to object
     *
     * @param $array array to convert to object
     * @return mixed return the array on object
     */
    public static function arrayToObject($array){
        return json_decode(json_encode($array));
    }


    /**
     *
     *
     */
    public function strongMeter($password)
    {
        $rank = 0;
        $rank += (strlen(trim($password)) > 7) ? 5 : 0;
        $rank += (strlen(trim($password)) > 11 && strlen(trim($password)) < 16) ? 10 : 0;
        $rank += (preg_match("/([A-Z])/", $password)) ? 10 : 0;
        $rank += (preg_match("/([0-9])/", $password)) ? 10 : 0;
    }

    /**
     * Metodo que construye el arbol de modulos del sistema
     *
     * @var $parent integer. ID del papa del módulo
     *
     * @return array
     */
    public static function moduleTree($parent = 0)
    {

        $response = [];

        foreach (SystemModule::where('parent', '=', $parent)->orderBy('order')->get() as $module) {

            $response[] = [
                'id'    => $module->id,
                'name'  => $module->name,
                'url'   => $module->url,
                'icon'  => $module->icon,
                'parent_as_child' => $module->parent_as_child,
                'child' => self::moduleTree($module->id)
            ];

        }

        return $response;

    }


    /**
     *
     *
     */
    public static function cleanPhone($phone)
    {
        $mapper = [
            '/',
            '(',
            ')',
            '-',
            ' '
        ];

        foreach ($mapper as $key => $value) {

            $phone = str_replace($value, "", $phone);

        }

        return $phone;
    }

    public static function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function getRandomWord($len = 5) {
        $word = array_merge(range('0', '9'), range('A', 'Z'));
        shuffle($word);
        return substr(implode($word), 0, $len);
    }

}
