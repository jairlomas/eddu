@extends('back.layout.dashboard')

{{-- Page Title --}}
@section('pageTitle')
    Alta de nuevo articulo
@stop

{{-- Content Title --}}
@section('contentTitle')
    Alta de nuevo articulo
@stop

{{-- Page Top Button --}}
@section('pageTopButton')
    <a href="{{ route('articulos_index') }}" class="btn btn-info"><i class="icofont icofont-rewind"></i> Regresar</a>
@stop

{{-- Main Content --}}
@section('mainContent')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Formulario de alta de nuevo articulo</h5>
                </div>

                <div class="card-block">

                    {{ Form::open(['route' => 'articulos.store']) }}

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Categoria</label>

                        <div class="col-sm-10">
                            {{ Form::select('category', $categories, null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Imagen de articulo</label>

                        <div class="col-sm-10">
                            {{ Form::file('imagen_articulo', null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Imagen de banner</label>

                        <div class="col-sm-10">
                            {{ Form::file('banner_articulo', null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Titulo</label>

                        <div class="col-sm-10">
                            {{ Form::text('titulo', null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Vista previa de contenido</label>

                        <div class="col-sm-10">
                            {{ Form::textarea('vista_previa_descripcion', null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Contenido</label>

                        <div class="col-sm-10">
                            <textarea name="decripcion_aux" id="decripcion_aux" rows="10" cols="80">
                            </textarea>
                        </div>
                    </div>

                    <textarea style="display: none;" name="descripcion" id="descripcion" rows="10" cols="80"></textarea>


                    <div class="form-group row">
                        <div class="col-sm-2"></div>

                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary btn-save"><i class="icofont icofont-save"></i> Guardar</button>
                            <button type="reset" class="btn btn-danger"><i class="icofont icofont-refresh"></i> Limpiar</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

{{-- Page JS --}}
@section('pageJS')
    {{ Html::script('/back/assets/js/ckeditor/ckeditor.js') }}
    {{ Html::script('/back/js/dashboard/simpleForm.js')  }}
    <script>
        $(document).ready(function () {
            CKEDITOR.replace( 'decripcion_aux');
            CKEDITOR.config.extraPlugins = 'colorbutton,colordialog';
        });
    </script>
@stop
