@extends('back.layout.dashboard')

{{-- Page Title --}}
@section('pageTitle')
    Alta de nueva palabra en diccionario
@stop

{{-- Content Title --}}
@section('contentTitle')
    Alta de nueva palabra en diccionario
@stop

{{-- Page Top Button --}}
@section('pageTopButton')
    <a href="{{ route('diccionario_index') }}" class="btn btn-info"><i class="icofont icofont-rewind"></i> Regresar</a>
@stop

{{-- Main Content --}}
@section('mainContent')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Formulario de alta de nueva palabra en diccionario</h5>
                </div>

                <div class="card-block">

                    {{ Form::open(['route' => 'diccionario.store']) }}

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Palabra</label>

                        <div class="col-sm-10">
                            {{ Form::text('palabra', null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Descripcion</label>

                        <div class="col-sm-10">
                            {{ Form::textarea('descripcion', null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-2"></div>

                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary btn-save"><i class="icofont icofont-save"></i> Guardar</button>
                            <button type="reset" class="btn btn-danger"><i class="icofont icofont-refresh"></i> Limpiar</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

{{-- Page JS --}}
@section('pageJS')
    {{ Html::script('/back/js/dashboard/simpleForm.js')  }}
@stop
