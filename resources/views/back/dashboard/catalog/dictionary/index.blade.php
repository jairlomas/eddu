@extends('back.layout.dashboard')

{{-- Page Title --}}
@section('pageTitle')
    Diccionario
@stop

{{-- Content Title --}}
@section('contentTitle')
    Catálogo de Palabras de Diccionario
@stop

{{-- Page Top Button --}}
@section('pageTopButton')
    <a href="{{ route('diccionario_create') }}" class="btn btn-info"><i class="icofont icofont-plus-circle"></i> Nueva palabra en diccionario</a>
@stop

{{-- Main Content --}}
@section('mainContent')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Tabla de palabras en diccionario registradas en el sistema</h5>
                </div>

                <div class="card-block">
                    <div class="dt-responsive table-responsive">
                        <table id="simpletable" class="table table-striped table-bordered nowrap">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Palabra</th>
                                <th>Descripcion</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{$item->word}}</td>
                                    <td>{{$item->description}}</td>
                                    </td>
                                    <td>
                                        <a href="{{URL::to('cms/dashboard/catalogo/diccionario/edit/'.base64_encode($item->id))}}" class="btn btn-warning"><i class="icofont icofont-pencil"></i>Editar</a>

                                        <a href="{{URL::to('cms/dashboard/catalogo/diccionario/delete/'.base64_encode($item->id))}}" class="btn btn-danger btn-delete" data-name='{{$item->word}}'><i class="icofont icofont-trash"></i>Borrar</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- Page JS --}}
@section('pageJS')
    {{ Html::script('back/js/dashboard/simpleTable.js')  }}
@stop
