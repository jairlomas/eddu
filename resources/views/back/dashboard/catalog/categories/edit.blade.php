@extends('back.layout.dashboard')

{{-- Page Title --}}
@section('pageTitle')
    Edición de categoria
@stop

{{-- Content Title --}}
@section('contentTitle')
    Edición de categoria
@stop

{{-- Page Top Button --}}
@section('pageTopButton')
    <a href="{{ route('categorias_index') }}" class="btn btn-info"><i class="icofont icofont-rewind"></i> Regresar</a>
@stop

{{-- Main Content --}}
@section('mainContent')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Formulario de edición de categoria</h5>
                </div>

                <div class="card-block">

                    {{ Form::open(['route' => array('categorias.update', base64_encode($data->id))]) }}


                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Imagen de banner principal</label>

                        <div class="col-sm-10">
                            <img src="{{ url($data->principal_banner) }}" style="width: 100%;" /><br>
                            {{ Form::file('banner_principal', null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    {{--<div class="form-group row">--}}
                        {{--<label class="col-sm-2 col-form-label">Imagen de banner en articulo</label>--}}

                        {{--<div class="col-sm-10">--}}
                            {{--<img src="{{ url($data->article_banner) }}" style="width: 100%;" /><br>--}}
                            {{--{{ Form::file('banner_articulo', null, ['class' => 'form-control']) }}--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Imagen de categoria</label>

                        <div class="col-sm-10">
                            <img src="{{ url($data->category_image) }}" style="width: 200px;" /><br>
                            {{ Form::file('imagen_categoria', null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Categoria</label>

                        <div class="col-sm-10">
                            {{ Form::text('categoria', $data->category_name, ['class' => 'form-control']) }}
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-sm-2"></div>

                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary"><i class="icofont icofont-save"></i> Guardar</button>
                            <button type="reset" class="btn btn-danger"><i class="icofont icofont-refresh"></i> Limpiar</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

{{-- Page JS --}}
@section('pageJS')
    {{ Html::script('/back/js/dashboard/simpleForm.js')  }}
@stop
