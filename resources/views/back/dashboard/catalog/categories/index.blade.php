@extends('back.layout.dashboard')

{{-- Page Title --}}
@section('pageTitle')
    Categorias
@stop

{{-- Content Title --}}
@section('contentTitle')
    Catálogo de Categorias
@stop

{{-- Page Top Button --}}
@section('pageTopButton')
    <a href="{{ route('categorias_create') }}" class="btn btn-info"><i class="icofont icofont-plus-circle"></i> Nueva categoria</a>
@stop

{{-- Main Content --}}
@section('mainContent')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Tabla de categorias registradas en el sistema</h5>
                </div>

                <div class="card-block">
                    <div class="dt-responsive table-responsive">
                        <table id="simpletable" class="table table-striped table-bordered nowrap">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Categoria</th>
                                <th>Imagen de banner principal</th>
                                {{--<th>Imagen de banner en articulo</th>--}}
                                <th>Imagen de categoria</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($data as $item)
                                <tr>





                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{$item->category_name}}</td>
                                    <td><img src="{{ url('/') }}/{{$item->principal_banner}}" style="width: 100%;" /></td>
{{--                                    <td><img src="{{ url('/') }}/{{$item->article_banner}}" style="width: 100%;" /></td>--}}
                                    <td><img src="{{ url('/') }}/{{$item->category_image}}" style="width: 100%;" /></td>
                                    <td>
                                        <a href="{{URL::to('cms/dashboard/catalogo/categorias/edit/'.base64_encode($item->id))}}" class="btn btn-warning"><i class="icofont icofont-pencil"></i>Editar</a>

                                        <a href="{{URL::to('cms/dashboard/catalogo/categorias/delete/'.base64_encode($item->id))}}" class="btn btn-danger btn-delete" data-name='{{$item->category_name}}'><i class="icofont icofont-trash"></i>Borrar</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- Page JS --}}
@section('pageJS')
    {{ Html::script('back/js/dashboard/simpleTable.js')  }}
@stop
