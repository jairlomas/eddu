

@extends('front.template.template')

@section('title', 'Eddu te aconseja')


@section('content')

    @section('pageCSS')
    @stop

    <div class="descripcion-articulo">

        <img src="{{ url($article->article_banner) }}" class="banner">

        <div class="container">

            <br>
            <div class="steps">
                <a href="{{ url('/') }}">Inicio</a>><a href="{{ url('eddu-te-aconseja') }}">Consejos</a>><a href="{{ url('eddu-te-aconseja/categoria/') }}/{{ $category->id }}">{{ $category->category_name }}</a>><a class="active">{{ $article->title }}</a>
            </div>
            <br class="hidden-tablet hidden-phone">
            <br>

            {!! $article->content !!}

            <br>
            <br>

        </div>

    </div>

    @section('pageSCRIPTS')
    @stop

@stop