

@extends('front.template.template')

@section('title', 'Eddu te aconseja')


@section('content')

    @section('pageCSS')
    @stop

    <div class="eddu-te-aconseja">
        <img src="{{ url('assets/img/front/eddu-te-aconseja/eddu-te-aconseja-banner-img.jpg') }}" class="banner">

        <section class="categories-section container text-center">
            <br>
            <h1 class="title">¿Tienes dudas? Descuida, aquí encontraras las respuestas</h1>
            <br>
            <br class="hidden-phone hidden-tablet">
            <p class="subtitle">Categorías</p>
            <br class="hidden-phone hidden-tablet">
            <p class="description">Selecciona la categoría de tu interés para ver consejos relacionados</p>
            <br class="hidden-phone hidden-tablet">
            <div class="row categorias">

                @foreach( $categories as $category )
                    <a class="col-6 col-sm-4 col-md-3 col-lg-3 col-xl-3" href="{{ url('eddu-te-aconseja/categoria/') }}/{{ $category->id }}">
                        <img src="{{ url($category->category_image) }}">
                        <p>{{ $category->category_name }}</p>
                    </a>
                @endforeach

            </div>
            <br>
        </section>

    </div>

    @section('pageSCRIPTS')
    @stop

@stop