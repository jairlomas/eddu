

@extends('front.template.template')

@section('title', 'Diccionario')


@section('content')

    @section('pageCSS')
    @stop

    <div class="diccionario">

        <img src="{{ url('assets/img/front/diccionario/diccionario-banner-img.jpg') }}" class="banner">

        <div class="container text-center">
            <br>
            <h1 class="title">¿Qué concepto deseas encontrar hoy?</h1>
            <br class="hidden-phone hidden-tablet">
            <br>
            <p class="subtitle">Buscador</p>
            <br class="hidden-phone hidden-tablet">
            <p class="description">Haz uso de nuestro motor de búsqueda para encontrar la definición del concepto que necesitas, o bien selecciona una inicial para mostrar un listado de términos que comiencen con dicha letra.</p>
            <br>
            <div class="input_search">
                {{ Form::open(array('url' => 'diccionario/palabra/', "type" => "GET", "id" => "form_search_word")) }}
                    <input type="text" value="" id="input_search" >
                    <button></button>
                {{ Form::close() }}
            </div>
            <br>
            <div class="row abecedario text-center">
                <a href="{{ url('diccionario/letra/A') }}" >A</a>
                <a href="{{ url('diccionario/letra/B') }}" >B</a>
                <a href="{{ url('diccionario/letra/C') }}" >C</a>
                <a href="{{ url('diccionario/letra/D') }}" >D</a>
                <a href="{{ url('diccionario/letra/E') }}" >E</a>
                <a href="{{ url('diccionario/letra/F') }}" >F</a>
                <a href="{{ url('diccionario/letra/G') }}" >G</a>
                <a href="{{ url('diccionario/letra/H') }}" >H</a>
                <a href="{{ url('diccionario/letra/I') }}" >I</a>
                <a href="{{ url('diccionario/letra/J') }}" >J</a>
                <a href="{{ url('diccionario/letra/K') }}" >K</a>
                <a href="{{ url('diccionario/letra/L') }}" >L</a>
                <a href="{{ url('diccionario/letra/M') }}" >M</a>
                <a href="{{ url('diccionario/letra/N') }}" >N</a>
                <a href="{{ url('diccionario/letra/O') }}" >O</a>
                <a href="{{ url('diccionario/letra/P') }}" >P</a>
                <a href="{{ url('diccionario/letra/Q') }}" >Q</a>
                <a href="{{ url('diccionario/letra/R') }}" >R</a>
                <a href="{{ url('diccionario/letra/S') }}" >S</a>
                <a href="{{ url('diccionario/letra/T') }}" >T</a>
                <a href="{{ url('diccionario/letra/U') }}" >U</a>
                <a href="{{ url('diccionario/letra/V') }}" >V</a>
                <a href="{{ url('diccionario/letra/W') }}" >W</a>
                <a href="{{ url('diccionario/letra/X') }}" >X</a>
                <a href="{{ url('diccionario/letra/Y') }}" >Y</a>
                <a href="{{ url('diccionario/letra/Z') }}" >Z</a>
            </div>

            <br>
            <div class="dots"></div>
            <br>
            <br>

            <div class="resultados text-left">

                <!-- BUSQUEDA POR LETRA -->
                @if( $type == "letra" )

                    <div class="resultado-buscador">
                        <p class="title">Resultados por iniciales</p>
                        <br class="hidden-tablet hidden-phone">
                        <div class="label_resultados">Se encontraron <span class="green_number" id="total_resultados_buscador">{{ count($data) }}</span> resultados para "<span class="palabra_buscar">{{ $word }}</span>"</div>
                        <br>

                        @if(count($data) > 0)
                            <section>
                                @foreach($data as $item)
                                    <article>
                                        <?php
                                            $word = $item["word"];
                                            $first_word = substr($word, 0, 1);
                                            $second_word = strtolower(substr($word, 1, strlen($word)));
                                        ?>
                                        <h3 class="title_diccionario"><span class="green_number">{{ $first_word }}</span>{{$second_word}}</h3>
                                        <p class="definition">{{ $item["description"] }}</p>
                                    </article>
                                @endforeach
                            </section>
                        @endif

                        <br>
                        <div class="dots"></div>
                    </div>

                @elseif( $type == "palabra" )

                <!-- BUSQUEDA POR PALABRA -->

                    <div class="resultado-buscador">
                        <p class="title">Resultados del buscador</p>
                        <br class="hidden-tablet hidden-phone">
                        @if($word == "all_words")
                            <div class="label_resultados">Se encontraron <span class="green_number" id="total_resultados_buscador">{{ count($data) }}</span> resultados</div>
                        @else
                            <div class="label_resultados">Se encontraron <span class="green_number" id="total_resultados_buscador">{{ count($data) }}</span> resultados para "<span class="palabra_buscar">{{ $word }}</span>"</div>
                        @endif
                        <br>

                        @if(count($data) > 0)
                            <section>
                                @foreach($data as $item)
                                    <article>
                                        <?php
                                            $item["word"] = ucfirst($item["word"]);
                                            $word_colored = str_ireplace($word, '<span class="green_number">'.$word.'</span>', $item["word"]);
                                            if( substr($word_colored, 0, 27) == '<span class="green_number">' ){
                                                $word_colored = str_ireplace($word, '<span class="green_number">'.ucfirst($word).'</span>', $item["word"]);
                                            }
                                        ?>

                                            <h3 class="title_diccionario">{!! $word_colored !!} </h3>
                                        <p class="definition">{{ $item["description"] }}</p>
                                    </article>
                                @endforeach
                            </section>
                        @endif

                        <br>
                        <div class="dots"></div>
                    </div>

                @endif

                <!--


                <br>

                <div class="resultado-iniciales">
                    <p class="title">Resultados por iniciales</p>
                    <br class="hidden-tablet hidden-phone">
                    <div class="label_resultados">Se encontraron <span class="green_number" id="total_resultados_iniciales">4</span> resultados para "<span class="palabra_buscar">A</span>"</div>
                    <br>
                    <section>
                        <article>
                            <h3 class="title_diccionario">Abono en cuenta</h3>
                            <p class="definition">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec.</p>
                        </article>
                        <article>
                            <h3 class="title_diccionario">Abono en cuenta</h3>
                            <p class="definition">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec.</p>
                        </article>
                        <article>
                            <h3 class="title_diccionario">Abono en cuenta</h3>
                            <p class="definition">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec.</p>
                        </article>
                        <article>
                            <h3 class="title_diccionario">Abono en cuenta</h3>
                            <p class="definition">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec.</p>
                        </article>                        
                    </section>
                    <br>
                    <div class="dots"></div>
                </div>
                -->

            </div>

        </div>

        <br>
        <br>

    </div>

    @section('pageSCRIPTS')
        <script>
            $(document).ready(function () {
                $(this).submit("#form_search_word", function (e) {
                    e.preventDefault();

                    var input_search = $("#input_search").val();
                    var url = $("#form_search_word").attr("action");

                    if(input_search == ""){
                        window.location.href = url + "/all_words";
                    }else{
                        window.location.href = url + "/" + input_search;
                    }


                });
            });
        </script>
    @stop

@stop