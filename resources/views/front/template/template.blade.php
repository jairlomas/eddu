
<html>
    <head>
        <title>{{ env('PROJECT_NAME') }} - @yield('title')</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" type="text/css" href="{{ url('assets/js/front/bootstrap-4.0.0-dist/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('assets/js/front/bootstrap-4.0.0-dist/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('assets/css/general.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('assets/css/general-responsive.css') }}">

        @section('pageCSS')
        @show

    </head>
    <body>

        <header>
            <div class="section_navbar">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ url('assets/img/front/navbar/eddu-navbar-id.svg') }}" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link @if( Request::is('/') ) active @endif" href="{{ url('') }}">Inicio</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if( Request::is('eddu-te-aconseja') || Request::is('eddu-te-aconseja/*') ) active @endif" href="{{ url('eddu-te-aconseja') }}">Eddu te aconseja</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if( Request::is('diccionario') ) active @endif" href="{{ url('diccionario') }}">Diccionario</a>
                            </li>
                        </ul>

                    </div>
                </nav>
            </div>
        </header>

        @yield('content')

        <div class="footer_section">
            <footer class="container-fluid">
                <div class="row">
                    <div class="col-md-3 text-left align-self-center">
                        <img class="fallabella-servicios-financieros" src="{{ url('assets/img/front/footer/falabella-servicios-financieros-footer-id.svg') }}" />
                    </div>
                    <article class="col-md-6 text-center align-self-center">
                        <div class="terminos">
                            <div>
                                <a href="#" class="">Aviso de privacidad</a>
                            </div>
                            <div>
                                <a href="#" class="">Protección de datos</a>
                            </div>
                            <div>
                                <a href="#" class="">Términos y Condiciones</a>
                            </div>
                        </div>
                    </article>
                    <div class="col-md-3 text-right align-self-center social-network">
                        <a href="#" class="facebook"></a>
                        <a href="#" class="instagram"></a>
                    </div>
                </div>
            </footer>
        </div>

        <script src="{{ url('assets/js/front/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ url('assets/js/front/popper.min.js') }}"></script>
        <script src="{{ url('assets/js/front/bootstrap-4.0.0-dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/front/general.js') }}"></script>

        @section('pageSCRIPTS')
        @show

    </body>
</html>