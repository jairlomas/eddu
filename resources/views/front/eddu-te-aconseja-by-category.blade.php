

@extends('front.template.template')

@section('title', 'Eddu te aconseja')


@section('content')

    @section('pageCSS')
    @stop

    <div class="eddu-te-categoria">

        <img src="{{ url($category->principal_banner) }}" class="banner">

        <div class="container">

            <br>
            <div class="steps">
                <a href="{{ url('/') }}">Inicio</a>><a href="{{ url('eddu-te-aconseja') }}">Consejos</a>><a class="active">Ahorro</a>
            </div>
            <br>
            <br class="hidden-phone hidden-tablet">

            <h1 class="title">{{ $category->category_name }}</h1>
            <br>

            <section class="articulos">

                @foreach( $articles as $article )
                    <article class="articulo row">
                        <div class="col-md-3 text-center">
                            <img src="{{ url($article->article_image) }}">
                        </div>
                        <div class="col-md-9">
                            <p class="title">{{ $article->title }}</p>
                            <p class="description">{{ $article->description_preview }}</p>
                            <a href="{{ url('eddu-te-aconseja/categoria/descripcion/') }}/{{ $article->id }}">Ver más</a>
                            <div class="dots"></div>
                        </div>
                    </article>
                @endforeach

                {{--<article class="articulo row">--}}
                    {{--<div class="col-md-3 text-center">--}}
                        {{--<img src="{{ url('assets/img/front/interior-ahorrar/ahorro-consejo-1-como-ahorrar-img.png') }}">--}}
                    {{--</div>--}}
                    {{--<div class="col-md-9">--}}
                        {{--<p class="title">¿Cómo ahorrar con la tarjeta de crédito?</p>--}}
                        {{--<p class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>--}}
                        {{--<a href="{{ url('eddu-te-aconseja/categoria/descripcion/1') }}">Ver más</a>--}}
                        {{--<div class="dots"></div>--}}
                    {{--</div>--}}
                {{--</article>--}}
                {{--<article class="articulo row">--}}
                    {{--<div class="col-md-3 text-center">--}}
                        {{--<img src="{{ url('assets/img/front/interior-ahorrar/ahorro-consejo-2-organizate-ahorra-img.png') }}">--}}
                    {{--</div>--}}
                    {{--<div class="col-md-9">--}}
                        {{--<p class="title">¿Cómo ahorrar con la tarjeta de crédito?</p>--}}
                        {{--<p class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>--}}
                        {{--<a href="{{ url('eddu-te-aconseja/categoria/descripcion/1') }}">Ver más</a>--}}
                        {{--<div class="dots"></div>--}}
                    {{--</div>--}}
                {{--</article>--}}
                {{--<article class="articulo row">--}}
                    {{--<div class="col-md-3 text-center">--}}
                        {{--<img src="{{ url('assets/img/front/interior-ahorrar/ahorro-consejo-3-deposito-plazo-img.png') }}">--}}
                    {{--</div>--}}
                    {{--<div class="col-md-9">--}}
                        {{--<p class="title">¿Cómo ahorrar con la tarjeta de crédito?</p>--}}
                        {{--<p class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>--}}
                        {{--<a href="{{ url('eddu-te-aconseja/categoria/descripcion/1') }}">Ver más</a>--}}
                        {{--<div class="dots"></div>--}}
                    {{--</div>--}}
                {{--</article>--}}

            </section>

            <br>
            <br>

        </div>

    </div>

    @section('pageSCRIPTS')
    @stop

@stop