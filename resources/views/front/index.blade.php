

@extends('front.template.template')

@section('title', 'Bienvenido')


@section('content')

    @section('pageCSS')
        <link rel="stylesheet" type="text/css" href="{{ url('assets/js/front/bxslider-4-4.2.12/dist/jquery.bxslider.min.css') }}">
    @stop



    <section class="slider_section">
        <div class="slider_home">
            <article class="slider_1">
                <div class="content_slider row">
                    <div class="col-md-6 text-right p-0">
                        <div class="dialogue-box" style="margin: 0 auto;">
                            <div class="dialogue-box-txt">
                                <p class="first-line">¡Hola México!</p>
                                <p class="second-line-small">SOY EDDU Y VINE PARA AYUDARTE</p>
                                <p class="third-line">A CUIDAR TU DINERO</p>
                            </div>

                            <div class="dialogue-box-direction-triangle"></div>
                        </div>
                    </div>
                    <div class="col-md-6 text-left p-0">
                        <img src="{{ url('assets/img/front/home/slide-1-eddu-perro-maleta-img.png') }}" class="character" />
                    </div>
                </div>
            </article>
            <article class="slider_2">
                <div class="content_slider row">
                    <div class="col-md-6 text-right">
                        <div class="dialogue-box dialogue-box-small-padding" style="margin: 0 auto;">
                            <div class="dialogue-box-txt">
                                <p class="first-line">Te proporcionamos la herramienta que necesitas</p>
                                <p class="second-line">PARA AYUDARTE A TENER</p>
                                <p class="third-line">FINANZAS MÁS SANAS</p>
                                <p class="description-line">Descarga el siguiente archivo y lleva el control de tus gastos</p>
                                <a href="." download>
                                    <button class="descarga-xlm">Descargar archivo .XML</button>
                                </a>
                            </div>

                            <div class="dialogue-box-direction-triangle"></div>
                        </div>
                    </div>
                    <div class="col-md-6 text-left">
                        <img src="{{ url('assets/img/front/home/slide-2-eddu-perro-tableta-img.png') }}" class="character" />
                    </div>
                </div>
            </article>
        </div>
    </section>

    @section('pageSCRIPTS')
        <script src="{{ url('assets/js/front/bxslider-4-4.2.12/dist/jquery.bxslider.min.js') }}"></script>
        <script src="{{ url('assets/js/front/home.js') }}"></script>
    @stop

@stop