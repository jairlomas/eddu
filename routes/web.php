<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Front\FrontController@index');
Route::get('eddu-te-aconseja', 'Front\FrontController@eddu_te_aconseja');
Route::get('eddu-te-aconseja/categoria/{id_category}', 'Front\FrontController@eddu_te_aconseja_by_category');
Route::get('eddu-te-aconseja/categoria/descripcion/{id_articulo}', 'Front\FrontController@descripcion_articulo_by_article');
Route::get('diccionario', 'Front\FrontController@diccionario');
Route::get('diccionario/{type}/{word}', 'Front\FrontController@diccionario');


/**
 * ------------------------------------------------------------------------
 * Admin Routes
 * ------------------------------------------------------------------------
 */
Route::group(['prefix' => 'cms'], function () {
    Route::get('/', 'Back\AccountController@getHome')->name('login');
    Route::get('/', 'Back\AccountController@getHome')->name('register');



    /**
     * ---------------------------------------------------------------------
     * Account Routes
     * ---------------------------------------------------------------------
     */
    Route::group(['prefix' => 'account'], function () {

        Route::get('/', function (){
            return redirect('cms');
        })->name('login');

        Route::get('logout', 'Back\AccountController@getLogout')->name('admin_logout');
        Route::post('login', 'Back\AccountController@postLogin')->name('admin_login');
        Route::get('password-recovery', 'Back\AccountController@getRecovery');
        Route::post('password-recovery', 'Back\AccountController@postRecovery');
        Route::get('password/reset/{token}', 'Back\AccountController@getReset')->name('password.request');
        Route::post('password/reset', 'Back\AccountController@postReset')->name('password.reset');
    });

    Route::group(['middleware' => ['auth']], function () {

        /**
         * ---------------------------------------------------------------------
         * Dashboard Routes
         * ---------------------------------------------------------------------
         */
        Route::group(['prefix' => 'dashboard'], function () {
            Route::view('/', 'back.dashboard.dashboard');

            /**
             * System Routes
             */
            Route::group(['prefix' => 'system'], function () {
                /**
                 * --------------------------------------------------
                 * User
                 * --------------------------------------------------
                 */
                Route::group(['prefix' => 'user'], function () {
                    Route::get('/', 'Back\Dashboard\System\UserController@index')->name('user_index');
                    Route::get('create', 'Back\Dashboard\System\UserController@create')->name('user_create_get');
                    Route::post('store', 'Back\Dashboard\System\UserController@store')->name('user_create');
                    Route::get('edit/{id}', 'Back\Dashboard\System\UserController@edit')->name('user.edit');
                    Route::post('update/{id}', 'Back\Dashboard\System\UserController@update')->name('user_update');
                    Route::get('delete/{id}', 'Back\Dashboard\System\UserController@destroy')->name('user_delete');
                });

                /**
                 * System User Profiles
                 *
                 */
                Route::group(['prefix' => 'profile'], function () {
                    Route::get('/', 'Back\Dashboard\System\ProfileController@index')->name('profile_index');
                    Route::get('create', 'Back\Dashboard\System\ProfileController@create')->name('profile_create');
                    Route::post('store', 'Back\Dashboard\System\ProfileController@store')->name('profile.store');
                    Route::get('edit/{id}', 'Back\Dashboard\System\ProfileController@edit')->name('profile.edit');
                    Route::post('update', 'Back\Dashboard\System\ProfileController@update')->name('profile.update');
                    Route::get('getData', 'Back\Dashboard\System\ProfileController@getData')->name('profile.data');
                    Route::get('view/{id}', 'Back\Dashboard\System\ProfileController@show')->name('profile.view');
                    Route::get('delete/{id}', 'Back\Dashboard\System\ProfileController@destroy')->name('profile.delete');
                });
            });


            /**
             * Catalog Routes
             */
            Route::group(['prefix' => 'catalogo'], function () {
                /**
                 * Catalog Categories
                 *
                 */
                Route::group(['prefix' => 'categorias'], function () {
                    Route::get('/', 'Back\Dashboard\Catalog\CategoriesController@index')->name('categorias_index');
                    Route::get('create', 'Back\Dashboard\Catalog\CategoriesController@create')->name('categorias_create');
                    Route::post('store', 'Back\Dashboard\Catalog\CategoriesController@store')->name('categorias.store');
                    Route::get('edit/{id}', 'Back\Dashboard\Catalog\CategoriesController@edit')->name('categorias.edit');
                    Route::post('update/{id}', 'Back\Dashboard\Catalog\CategoriesController@update')->name('categorias.update');
                    Route::get('getData', 'Back\Dashboard\Catalog\CategoriesController@getData')->name('categorias.data');
                    Route::get('view/{id}', 'Back\Dashboard\Catalog\CategoriesController@show')->name('categorias.view');
                    Route::get('delete/{id}', 'Back\Dashboard\Catalog\CategoriesController@destroy')->name('categorias.delete');
                });

                /**
                 * Catalog Articulos
                 *
                 */
                Route::group(['prefix' => 'articulos'], function () {
                    Route::get('/', 'Back\Dashboard\Catalog\ArticleController@index')->name('articulos_index');
                    Route::get('create', 'Back\Dashboard\Catalog\ArticleController@create')->name('articulos_create');
                    Route::post('store', 'Back\Dashboard\Catalog\ArticleController@store')->name('articulos.store');
                    Route::get('edit/{id}', 'Back\Dashboard\Catalog\ArticleController@edit')->name('articulos.edit');
                    Route::post('update/{id}', 'Back\Dashboard\Catalog\ArticleController@update')->name('articulos.update');
                    Route::get('getData', 'Back\Dashboard\Catalog\ArticleController@getData')->name('articulos.data');
                    Route::get('view/{id}', 'Back\Dashboard\Catalog\ArticleController@show')->name('articulos.view');
                    Route::get('delete/{id}', 'Back\Dashboard\Catalog\ArticleController@destroy')->name('articulos.delete');
                });

                /**
                 * Catalog Articulos
                 *
                 */
                Route::group(['prefix' => 'diccionario'], function () {
                    Route::get('/', 'Back\Dashboard\Catalog\DictionaryController@index')->name('diccionario_index');
                    Route::get('create', 'Back\Dashboard\Catalog\DictionaryController@create')->name('diccionario_create');
                    Route::post('store', 'Back\Dashboard\Catalog\DictionaryController@store')->name('diccionario.store');
                    Route::get('edit/{id}', 'Back\Dashboard\Catalog\DictionaryController@edit')->name('diccionario.edit');
                    Route::post('update/{id}', 'Back\Dashboard\Catalog\DictionaryController@update')->name('diccionario.update');
                    Route::get('getData', 'Back\Dashboard\Catalog\DictionaryController@getData')->name('diccionario.data');
                    Route::get('view/{id}', 'Back\Dashboard\Catalog\DictionaryController@show')->name('diccionario.view');
                    Route::get('delete/{id}', 'Back\Dashboard\Catalog\DictionaryController@destroy')->name('diccionario.delete');
                });

            });


        });
    });
});

